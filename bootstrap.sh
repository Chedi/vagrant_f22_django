#!/usr/bin/env bash

#Exporting the environment variables from puppet
cat <<EOF >> /etc/bashrc
export XANADOU_PATH="$1"
export XANADOU_SOURCE="$2"
export XANADOU_APP_NAME="$3"
export XANADOU_PYTHON_3="$4"
export XANADOU_EXTRA_INFO="$5"
export XANADOU_SERVER_NAME="$6"
EOF

source /etc/bashrc

#Adding 1GB of swap
echo "Adding 1GB of swap space"
dd if=/dev/zero of=/var/swap bs=1M count=1000 &> /dev/null
chmod 600 /var/swap
mkswap /var/swap
swapon /var/swap
echo "/var/swap    swap    swap    defaults    1 1" >> /etc/fstab

#Updating the system and installing the server pacakges
echo "Updating the system"
dnf update  -y

echo "Installing basic server packages"
dnf install -y puppet python3 nginx redis postgresql-server postgresql htop \
    postgresql-devel postgresql-contrib python3-devel supervisor python3-virtualenv

#Making a link to the python3 virtualenv (used by puppet package)
echo "Creating a link for the virtualenv-3"
ln /usr/bin/py3-virtualenv /usr/bin/virtualenv-3

#Installing the puppet django module
echo "Installing the chedi-django puppet module"
puppet module install chedi-django

#Starting the puppet setup
echo "Starting puppet environment setup"
puppet apply /vagrant/manifests/site.pp --verbose --graph --graphdir /tmp

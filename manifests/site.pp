class { 'django':
  path        => $::xanadou_path,
  source      => $::xanadou_source,
  app_name    => $::xanadou_app_name,
  python_3    => $::xanadou_python_3,
  extra_info  => $::xanadou_extra_info,
  server_name => $::xanadou_server_name,
}
